﻿/*
 ***********************************************************   
 *   AUTHOR:   Uzair Z Sheikh
 *   Position: Senior Product Specialist
 *   COMPANY:  Fortrus Limited
 *   Date:     25/05/2014
 *********************************************************** 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;

namespace AutomatedDocLSTGenerator
{
    public partial class Configurations : Form
    {
        string folderPath = string.Empty;
        string DocFilePath;
        /*
         *  Variables to save LorenIpsum values
         */
        string HospNoRand = string.Empty;    /* HosNoRand saving LorenIpsum value for HospNo*/
        string SurnameRand = string.Empty;   /* SurnameRand saving LorenIpsum value for SurnameRand*/
        string FirstNameRand = string.Empty; /* FirsNameRand saving LorenIpsum value for FirsNameRand*/
        string AddressRand = string.Empty;   /* CountryRand saving LorenIpsum value for CountryRand*/
        string BodyTextRand = string.Empty;  /* TextBodyRand saving LorenIpsum value for TextBodyRand*/
        
        /* Variable to save all the LorenIpsum text from Website */
        string word; 
        string[] separators = { ",", ".", "!", "?", ";", ":", " ", "\n" };
        string[] LorenIpsumData = new string[0];

        
        string[] BodyTxt;

        public Configurations()
        {
            InitializeComponent();
        }

        private void Configurations_Load(object sender, EventArgs e)
        {
            btnStart.Enabled = false;
            /*
             * Load LoremIpsum data just once
             * for BodyText
             */            
            BodyTxt = File.ReadAllLines(".\\Resources\\LoremIpsumData.txt");
            StringBuilder stringbuilder = new StringBuilder();
            foreach (string value in BodyTxt)
            {
                stringbuilder.Append(value);
            }
            word = stringbuilder.ToString();
            LorenIpsumData = word.Split(separators, StringSplitOptions.RemoveEmptyEntries);
        }
            
        private void btnStart_Click(object sender, EventArgs e)
        {
            if (txtTotalDocs.Text.Length < 1 && folderPath.Length < 1)
            {
                MessageBox.Show("Total Documnets & Folder Path required");                
            }
            else if (txtTotalDocs.Text.Length < 1)
            {
                MessageBox.Show("Total Documents required");
            }
            else if (folderPath.Length < 1)
            {
                MessageBox.Show("Folder Path required");
            }
            else
            {
                /*
             *  Get total documents to create value from textbox
             */
                int TotalDocs = Convert.ToInt32(txtTotalDocs.Text);
                for (int i = 1; i <= TotalDocs; i++)
                {
                    /*
                     * Call Doc method to create Document first
                     * Call LST method to create LST after Doc created
                     */
                    GenerateDocs("Sample CDM Doc " + i);
                    GenereateLST("Sample CDM Doc " + i);
                }

                MessageBox.Show("Automated Documents Generation Completed");
            }            
        }

        private void GenerateDocs(string documentFileName)
        {
            /*
             * LoremImsumxxx(minWords, maxWords, minSentenses, maxSentenses, numParagraphs)
             */
            LoremIpsumHospNo(1, 1, 1, 1, 1);
            LoremIpsumSurname(1, 1, 1, 1, 1);
            LoremIpsumFirstname(1, 1, 1, 1, 1);
            LoremIpsumAddress(1, 1, 1, 1, 1);
            LoremIpsumBodyText(20, 50, 1, 5, 2);

            int NoOfPages = Convert.ToInt32(txtNoOfPages.Text.ToString());
            int pageBreakCheck = 0;
            DocFilePath = folderPath + "\\" + documentFileName + ".docx";
            using (WordprocessingDocument myDoc =
                   WordprocessingDocument.Create(DocFilePath,
                                 WordprocessingDocumentType.Document))
            {
                MainDocumentPart mainPart = myDoc.AddMainDocumentPart();
                mainPart.Document = new Document();                
                
                do
                {
                    Body body = new Body();
                    Paragraph paragraph = new Paragraph();
                    Run run_paragraph = new Run();

                    if (pageBreakCheck > 0)
                    {
                        paragraph = new Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new DocumentFormat.OpenXml.Wordprocessing.Break() { Type = BreakValues.Page }));
                    }
                    
                    Text text_paragraph = new Text("Hospital Number:  " + HospNoRand + "\n" + "Surname:  " + SurnameRand + "\n" + "Firstname:  " + FirstNameRand + "\n" + "Address:  " + AddressRand + "\n" + "Subject:  CDM Sample Document" + "\n" + "\n" + BodyTextRand);
                    run_paragraph.Append(text_paragraph);
                    paragraph.Append(run_paragraph);
                    body.Append(paragraph);
                    mainPart.Document.Append(body);
                    mainPart.Document.Save();
                    NoOfPages--;
                    /*
                     * If the loop runs again, this means a multipage Doc needs tobe created
                     * In case case, we will add the PageBreak at the top of this loop
                     */
                    pageBreakCheck++;
                }
                while (NoOfPages > 0);

                
            }
        }

        private void GenereateLST(string documentFileName)
        {
            /*
             * Pass data for LST file in LSTData Array
             *                &
             * Create LST with data in LSTData Array
             */
            string[] LSTData = { "LASERFICHE IMPORT LIST","Document(" +HospNoRand+ ")", "STARTFIELDS", "OperationalRecords", "ENDFIELDS", "STARTLIST", folderPath + "\\" + documentFileName + ".docx", "ENDLIST" };
            System.IO.File.WriteAllLines(folderPath + "\\" + documentFileName + ".lst", LSTData);
        }
                
        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {
            /*
             * This function gets called at 'Choose Location' button click
             */
            /*
             * Browse to select the destination folder
             */
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                folderPath = folderBrowserDialog1.SelectedPath;
                lblFolderPath.Text = folderPath;
            }
        }

        
        private void txtTotalDocs_KeyUp(object sender, KeyEventArgs e)
        {
            /*
             * Applying validation on TotalDocs & NoOfPages
             * As the values should be digits only (No Char)
             */
            var regexItem = new Regex("^[0-9 ]*$");
            if (regexItem.IsMatch(txtTotalDocs.Text) || txtTotalDocs.Text.Length > 0)
            {
                btnStart.Enabled = true;
            }
            else
            {
                btnStart.Enabled = false;
                MessageBox.Show("Invalid Documents Number");
            }
        }

        private void txtNoOfPages_KeyUp(object sender, KeyEventArgs e)
        {
            var regexItem = new Regex("^[0-9 ]*$");
            if (regexItem.IsMatch(txtNoOfPages.Text))
            {
                btnStart.Enabled = true;
            }
            else
            {
                btnStart.Enabled = false;
                MessageBox.Show("Invalid No of Pages");
            }
        }

        public void LoremIpsumHospNo(int minWords, int maxWords,
            int minSentences, int maxSentences,
            int numParagraphs)
        {
            HospNoRand = string.Empty;
            string[] HospNos;
            HospNos = File.ReadAllLines(".\\Resources\\HospitalNumbers.txt");
            
            var rand = new Random();
                int numSentences = rand.Next(maxSentences - minSentences)
                    + minSentences;// +1;
                int numWords = rand.Next(maxWords - minWords) + minWords;// +1;

                for (int p = 0; p < numParagraphs; p++)
                {
                    for (int s = 0; s < numSentences; s++)
                    {
                        for (int w = 0; w < numWords; w++)
                        {
                            if (w > 0) { HospNoRand += " \n"; }
                            HospNoRand += HospNos[rand.Next(HospNos.Length)];
                        }
                        HospNoRand += "\n ";
                    }
                }
        }

        public void LoremIpsumSurname(int minWords, int maxWords,
            int minSentences, int maxSentences,
            int numParagraphs)
        {
            SurnameRand = string.Empty;
            string[] Surname;
            Surname = File.ReadAllLines(".\\Resources\\NamesLast.txt");

            var rand = new Random();
            int numSentences = rand.Next(maxSentences - minSentences)
                + minSentences;// +1;
            int numWords = rand.Next(maxWords - minWords) + minWords;// +1;

            for (int p = 0; p < numParagraphs; p++)
            {
                for (int s = 0; s < numSentences; s++)
                {
                    for (int w = 0; w < numWords; w++)
                    {
                        if (w > 0) { SurnameRand += " \n"; }
                        SurnameRand += Surname[rand.Next(Surname.Length)];
                    }
                    SurnameRand += "\n ";
                }
            }
        }

        public void LoremIpsumFirstname(int minWords, int maxWords,
            int minSentences, int maxSentences,
            int numParagraphs)
        {
            FirstNameRand = string.Empty;
            string[] FirstNames;
            FirstNames = File.ReadAllLines(".\\Resources\\NamesFirstFemale.txt");

            var rand = new Random();
            int numSentences = rand.Next(maxSentences - minSentences)
                + minSentences;// +1;
            int numWords = rand.Next(maxWords - minWords) + minWords;// +1;

            for (int p = 0; p < numParagraphs; p++)
            {
                for (int s = 0; s < numSentences; s++)
                {
                    for (int w = 0; w < numWords; w++)
                    {
                        if (w > 0) { FirstNameRand += " \n"; }
                        FirstNameRand += FirstNames[rand.Next(FirstNames.Length)];
                    }
                    FirstNameRand += "\n ";
                }
            }
        }

        public void LoremIpsumAddress(int minWords, int maxWords,
            int minSentences, int maxSentences,
            int numParagraphs)
        {
            AddressRand = string.Empty;
            string[] Address;
            Address = File.ReadAllLines(".\\Resources\\USCity.txt");

            var rand = new Random();
            int numSentences = rand.Next(maxSentences - minSentences)
                + minSentences;// +1;
            int numWords = rand.Next(maxWords - minWords) + minWords;// +1;

            for (int p = 0; p < numParagraphs; p++)
            {
                for (int s = 0; s < numSentences; s++)
                {
                    for (int w = 0; w < numWords; w++)
                    {
                        if (w > 0) { AddressRand += " \n"; }
                        AddressRand += Address[rand.Next(Address.Length)];
                    }
                    AddressRand += "\n ";
                }
            }
        }

        public void LoremIpsumBodyText(int minWords, int maxWords,
            int minSentences, int maxSentences,
            int numParagraphs)
        {

            BodyTextRand = string.Empty;
            var rand = new Random();
            int numSentences = rand.Next(maxSentences - minSentences)
                + minSentences;// +1;
            int numWords = rand.Next(maxWords - minWords) + minWords;// +1;

            for (int p = 0; p < numParagraphs; p++)
            {
                for (int s = 0; s < numSentences; s++)
                {
                    for (int w = 0; w < numWords; w++)
                    {
                        if (w > 0) { BodyTextRand += " \n"; }
                        BodyTextRand += LorenIpsumData[rand.Next(LorenIpsumData.Length)] + "  ";
                    }
                    BodyTextRand += " \n  ";
                }
            }
        }
                
        public void LoremIpsum()
        {
            /*
             * Get the LoremIpsum date live
             */
            /*WebRequest request = WebRequest.Create("http://lipsum.com/feed/html");
            request.Credentials = CredentialCache.DefaultCredentials;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            HTML = reader.ReadToEnd(); //se citeste codul HTMl

            //searching for Lorem Ipsum
            HTML = HTML.Remove(0, HTML.IndexOf("<div id=\"lipsum\">"));
            HTML = HTML.Remove(HTML.IndexOf("</div>"));
            HTML = HTML
                 .Replace("<div id=\"lipsum\">", "")
                 .Replace("</div>", "")
                 .Replace("<p>", "")
                 .Replace("</p>", "");

            reader.Close();
            dataStream.Close();
            response.Close();*/
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
    }
}
