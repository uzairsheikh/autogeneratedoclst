﻿/*
 ***********************************************************   
 *   AUTHOR:   Uzair Z Sheikh
 *   Position: Senior Product Specialist
 *   COMPANY:  Fortrus Limited
 *   Date:     25/05/2014
 *********************************************************** 
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutomatedDocLSTGenerator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Configurations());
        }
    }
}
