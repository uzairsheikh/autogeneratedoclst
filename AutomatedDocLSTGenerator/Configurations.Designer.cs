﻿/*
 ***********************************************************   
 *   AUTHOR:   Uzair Z Sheikh
 *   Position: Senior Product Specialist
 *   COMPANY:  Fortrus Limited
 *   Date:     25/05/2014
 *********************************************************** 
 */

namespace AutomatedDocLSTGenerator
{
    partial class Configurations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSaveLocation = new System.Windows.Forms.Button();
            this.txtTotalDocs = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.lblFolderPath = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNoOfPages = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnSaveLocation
            // 
            this.btnSaveLocation.Location = new System.Drawing.Point(141, 99);
            this.btnSaveLocation.Name = "btnSaveLocation";
            this.btnSaveLocation.Size = new System.Drawing.Size(100, 23);
            this.btnSaveLocation.TabIndex = 3;
            this.btnSaveLocation.Text = "Choose Location";
            this.btnSaveLocation.UseVisualStyleBackColor = true;
            this.btnSaveLocation.Click += new System.EventHandler(this.folderBrowserDialog1_HelpRequest);
            // 
            // txtTotalDocs
            // 
            this.txtTotalDocs.Location = new System.Drawing.Point(141, 47);
            this.txtTotalDocs.Name = "txtTotalDocs";
            this.txtTotalDocs.Size = new System.Drawing.Size(100, 20);
            this.txtTotalDocs.TabIndex = 1;
            this.txtTotalDocs.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTotalDocs_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Total Documents:";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(189, 160);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 4;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.HelpRequest += new System.EventHandler(this.folderBrowserDialog1_HelpRequest);
            // 
            // lblFolderPath
            // 
            this.lblFolderPath.AutoSize = true;
            this.lblFolderPath.Location = new System.Drawing.Point(78, 134);
            this.lblFolderPath.Name = "lblFolderPath";
            this.lblFolderPath.Size = new System.Drawing.Size(0, 13);
            this.lblFolderPath.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(108, 160);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "No of Pages:";
            // 
            // txtNoOfPages
            // 
            this.txtNoOfPages.Location = new System.Drawing.Point(141, 73);
            this.txtNoOfPages.Name = "txtNoOfPages";
            this.txtNoOfPages.Size = new System.Drawing.Size(100, 20);
            this.txtNoOfPages.TabIndex = 2;
            this.txtNoOfPages.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtNoOfPages_KeyUp);
            // 
            // Configurations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 221);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNoOfPages);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblFolderPath);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTotalDocs);
            this.Controls.Add(this.btnSaveLocation);
            this.Name = "Configurations";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Automated Doc & LST Generator";
            this.Load += new System.EventHandler(this.Configurations_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSaveLocation;
        private System.Windows.Forms.TextBox txtTotalDocs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label lblFolderPath;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNoOfPages;
    }
}

